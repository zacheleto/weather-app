# Weather APP

A simple weather app that can fetch data from an API and display weather info about cities all around the Globe, It was meant to be simple and quick which exactly what it does.  

### Prerequisites

none

### Installing

git clone https://github.com/letowebdev/Weather-app.git

## Deployment

http://zacheabdelatif.website/Projects/weatherapp

## Built With

* [JavaScript ES6 & ES7]
* [1 line code of jQuery]
* [openweather API]

## Author

* **Zache Abdelatif (Leto)**

## License

This project is licensed under the MIT License
