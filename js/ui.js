class UI {

constructor()
{
    this.location = document.getElementById('w-location');
    this.desc = document.getElementById('w-desc');
    this.string = document.getElementById('w-string');
    this.details = document.getElementById('w-details');
    this.icon = document.getElementById('w-icon');
    this.humidity = document.getElementById('w-humidity');
    this.feelsLike = document.getElementById('w-feels-like');
    this.temMin = document.getElementById('w-tem-min');
    this.temMax = document.getElementById('w-tem-max');
    this.wind = document.getElementById('w-wind');
}

display(weather)
{
    this.location.textContent = weather.name + ' - ' + weather.sys.country;
    this.desc.textContent = weather.weather[0].main + ': ' + weather.weather[0].description;
    this.string.textContent = weather.main.temp + ' °C';
    this.icon.setAttribute('src', `http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`);
    this.humidity.textContent = `Humidity: ${weather.main.humidity}`;
    this.feelsLike.textContent = `Feels like: ${weather.main.feels_like} °C`;
    this.temMin.textContent = `Minimum tempreture: ${weather.main.temp_min} °C`;
    this.temMax.textContent = `Maximum tempreture: ${weather.main.temp_max} °C`;
    this.wind.textContent = `Wind speed: ${weather.wind.speed} MPH`;




}
 

}