class Weather {
    constructor(city, country)
    {
        this.apiKey = '9e021715118db8337938c8c3508e7307';
        this.city = city;
        this.country = country;
    }

    // Fetching weather from API
    async getWeather()
    {
        const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.country}&units=metric&appid=${this.apiKey}`);

        const responseData = await response.json();
        return responseData;
    }

    // Change location
    ChangeLocation(city, country)
    {
        this.city = city;
        this.country = country;
    }






}