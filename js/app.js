// init classes
const ui = new UI;
const storage = new Storage;
//get sorage location data
const weatherLocation = storage.getLocationData();
const weather = new Weather(weatherLocation.city, weatherLocation.country);

// Get Weather on DOM load
document.addEventListener('DOMContentLoaded', getWeather);

// Change location event
document.getElementById('w-change-btn').addEventListener('click', (e) =>{
    const city = document.getElementById('city').value;
    const country = document.getElementById('country').value;

    //change location
    weather.ChangeLocation(city, country);

    // Set location in LS
    storage.setLocationData(city, country)
    //get and display weather
    getWeather();

    //close Modal
    $('#locModal').modal('hide');

    

})

//Get weather DATA from API
function getWeather()
{
weather.getWeather()
.then(results => {
    console.log(results);
    ui.display(results);
})
.catch(err => console.log(err));
}

